import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutternewsapp/app/res/routeGenerator.dart';

import 'app/res/Themes.dart';
import 'app/ui/pages/FingerPrintAuth/FingerPrintAuth_bloc.dart';
import 'app/ui/pages/NewsHomeScreen/NewsHomeScreen_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      child: MaterialApp(
        theme: AppTheme.currentTheme,
        initialRoute: '/Auth',
        onGenerateRoute: RouteGenerator.generateRoute,
        debugShowCheckedModeBanner: false,
      ),
      blocs: [
        Bloc(
          (i) => FingerPrintAuthBloc(),
        ),
        Bloc(
          (i) => NewsHomeScreenBloc(),
        ),
      ],
    );
  }
}
