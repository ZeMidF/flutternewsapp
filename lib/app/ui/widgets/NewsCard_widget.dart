import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutternewsapp/app/res/strings.dart';
import 'package:flutternewsapp/app/res/utils.dart';
import 'package:flutternewsapp/app/ui/widgets/LoadingWidget_widget.dart';

class NewsCard extends StatelessWidget {
  final String urlToImage, url, title, source, description;
  final DateTime date;
  NewsCard({
    this.title = "",
    this.description = "",
    this.urlToImage = "",
    this.url = "",
    this.source = "",
    this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      child: Container(
        height: 110,
        padding: EdgeInsets.all(5.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      title,
                      style: Theme.of(context).textTheme.body1,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                    margin: EdgeInsets.only(bottom: 10.0),
                  ),
                  Text(
                    Utils.getDiferenceToCurrentInHours(date) == 0
                        ? AppStrings.justNow
                        : "${Utils.getDiferenceToCurrentInHours(date)} " + AppStrings.hoursAgo,
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Hero(
                      tag: "heroNewsDetailPage$url",
                      child: CachedNetworkImage(
                        imageUrl: urlToImage != null ? urlToImage : "",
                        imageBuilder: (context, imageProvider) {
                          return Container(
                            width: 72.0,
                            height: 72.0,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                        placeholder: (context, url) => Container(
                          width: 72.0,
                          height: 72.0,
                          child: LoadingWidget(),
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          'assets/images/error.png',
                          fit: BoxFit.cover,
                          width: 72.0,
                          height: 72.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
