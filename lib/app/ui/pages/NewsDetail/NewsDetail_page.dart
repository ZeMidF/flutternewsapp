import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutternewsapp/app/data/model/NewsApiArticleModel_model.dart';
import 'package:flutternewsapp/app/res/strings.dart';
import 'package:flutternewsapp/app/res/utils.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsDetailPage extends StatefulWidget {
  final Articles currentArticle;
  const NewsDetailPage({Key key, this.currentArticle}) : super(key: key);

  @override
  _NewsDetailPageState createState() => _NewsDetailPageState();
}

class _NewsDetailPageState extends State<NewsDetailPage> {
  @override
  Widget build(BuildContext context) {
    double imageHeight = MediaQuery.of(context).size.height * 0.35;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.currentArticle.source.name,
          style: Theme.of(context).textTheme.title,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Hero(
              tag: "heroNewsDetailPage${widget.currentArticle.url}", // using url to HeroTag
              child: CachedNetworkImage(
                imageUrl: widget.currentArticle.urlToImage != null ? widget.currentArticle.urlToImage : "",
                fit: BoxFit.fitWidth,
                imageBuilder: (context, imageProvider) {
                  return Container(
                    height: imageHeight,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
                placeholder: (context, url) => Container(
                  height: imageHeight,
                  child: Center(
                    child: Platform.isAndroid ? CircularProgressIndicator() : CupertinoActivityIndicator(),
                  ),
                ),
                errorWidget: (context, url, error) => Image.asset(
                  'assets/images/error.png',
                  fit: BoxFit.cover,
                  height: imageHeight,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Text(
                    widget.currentArticle.title,
                    style: Theme.of(context).textTheme.headline,
                  ),
                  SizedBox(height: 10),
                  Text(
                    "${Utils.getDateString(widget.currentArticle.publishedAt, dateFormat: "HH'H':mm'm' - dd-MM-yyyy ")}",
                    style: Theme.of(context).textTheme.body2,
                  ),
                  SizedBox(height: 10),
                  RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.body2,
                      children: [
                        TextSpan(text: "by "),
                        TextSpan(
                          text: "${widget.currentArticle.author}",
                          style: Theme.of(context).textTheme.body2.copyWith(color: Colors.red),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Column(
                    children: <Widget>[
                      Container(
                        child: (widget.currentArticle.description != null && widget.currentArticle.description.length > 0)
                            ? Text(
                                widget.currentArticle.description,
                                style: Theme.of(context).textTheme.body2,
                              )
                            : null,
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        child: (widget.currentArticle.content != null && widget.currentArticle.content.length > 0)
                            ? Text(
                                widget.currentArticle.content,
                                style: Theme.of(context).textTheme.body2,
                              )
                            : null,
                      ),
                    ],
                  ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        child: new Text(
                          AppStrings.goToFullArticle,
                          style: Theme.of(context).textTheme.title,
                        ),
                        onPressed: () async {
                          if (await canLaunch(widget.currentArticle.url)) {
                            await launch(widget.currentArticle.url);
                          }
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 30),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
