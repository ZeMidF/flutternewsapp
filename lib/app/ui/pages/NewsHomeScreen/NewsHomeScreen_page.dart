import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutternewsapp/app/res/strings.dart';
import 'package:flutternewsapp/app/ui/widgets/LoadingWidget_widget.dart';
import 'package:flutternewsapp/app/ui/widgets/NewsCard_widget.dart';

import 'NewsHomeScreen_bloc.dart';

class NewsHomeScreenPage extends StatefulWidget {
  const NewsHomeScreenPage({Key key}) : super(key: key);

  @override
  _NewsHomeScreenPageState createState() => _NewsHomeScreenPageState();
}

class _NewsHomeScreenPageState extends State<NewsHomeScreenPage> {
  NewsHomeScreenBloc newsBloc;

  @override
  void initState() {
    try {
      newsBloc = BlocProvider.getBloc<NewsHomeScreenBloc>();
      newsBloc.getSources();
    } catch (e) {}
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Consumer<NewsHomeScreenBloc>(
          builder: (BuildContext context, NewsHomeScreenBloc bloc) {
            return Text(
              bloc.currentSelectedSource != null ? bloc.currentSelectedSource.name : AppStrings.newsHomeScreenTitle,
              style: Theme.of(context).textTheme.title,
            );
          },
        ),
      ),
      body: Consumer<NewsHomeScreenBloc>(
        builder: (BuildContext context, NewsHomeScreenBloc newsBloc) {
          if (newsBloc.sourcesList != null && newsBloc.sourcesList.length > 0) {
            return Column(
              children: <Widget>[
                Container(
                  height: 70,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: HeaderSourcesWidget(
                      newsBloc: newsBloc,
                    ),
                  ),
                ),
                Expanded(
                  child: newsBloc.newsList != null && newsBloc.newsList.length > 0
                      ? BodyNewsWidget(
                          newsBloc: newsBloc,
                        )
                      : LoadingWidget(),
                ),
              ],
            );
          } else {
            return LoadingWidget();
          }
        },
      ),
    );
  }
}

class BodyNewsWidget extends StatelessWidget {
  final NewsHomeScreenBloc newsBloc;

  const BodyNewsWidget({
    Key key,
    @required this.newsBloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: newsBloc.newsList.length,
      itemBuilder: (context, index) {
        var currentArticle = newsBloc.newsList[index];
        return GestureDetector(
          onTap: () async {
            Navigator.of(context).pushNamed(
              '/NewsDetais',
              arguments: currentArticle,
            );
          },
          child: NewsCard(
            source: currentArticle.source?.name,
            description: currentArticle.description,
            title: currentArticle.title,
            url: currentArticle.url,
            urlToImage: currentArticle.urlToImage,
            date: currentArticle.publishedAtDateTime,
          ),
        );
      },
    );
  }
}

class HeaderSourcesWidget extends StatelessWidget {
  final NewsHomeScreenBloc newsBloc;

  const HeaderSourcesWidget({
    Key key,
    @required this.newsBloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey.shade50,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: newsBloc.sourcesList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          var currentSource = newsBloc.sourcesList[index];
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ActionChip(
              backgroundColor: Colors.blueGrey[200],
              onPressed: () {
                newsBloc.getNewsBySource(currentSource);
              },
              avatar: CircleAvatar(
                backgroundColor: Colors.grey.shade800,
                child: Text(currentSource.name.toUpperCase()[0]),
              ),
              label: Text(currentSource.name),
            ),
          );
        },
      ),
    );
  }
}
