import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutternewsapp/app/data/api/NewsApi_repository.dart';
import 'package:flutternewsapp/app/data/model/NewsApiArticleModel_model.dart';
import 'package:flutternewsapp/app/data/model/NewsApiSourceModel_model.dart';

class NewsHomeScreenBloc extends BlocBase {
  List<Articles> newsList = List<Articles>();
  List<Sources> sourcesList = List<Sources>();
  Sources currentSelectedSource;

  getNewsBySource(Sources source) async {
    currentSelectedSource = source;
    newsList = await NewsApiRepositoryImpl().getNewsBySource(source.id);
    newsList.sort((a, b) => a.compareTo(b));
    notifyListeners();
  }

  getSources() async {
    sourcesList = await NewsApiRepositoryImpl().getSources();
    if (sourcesList.length > 0) {
      try {
        getNewsBySource(sourcesList[0]);
      } catch (e) {}
    }
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
