import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutternewsapp/app/ui/pages/FingerPrintAuth/FingerPrintAuth_bloc.dart';
import 'package:flutternewsapp/app/ui/widgets/LoadingWidget_widget.dart';

class FingerPrintAuthPage extends StatefulWidget {
  final String title;
  const FingerPrintAuthPage({Key key, this.title = "FingerPrintAuth"}) : super(key: key);

  @override
  _FingerPrintAuthPageState createState() => _FingerPrintAuthPageState();
}

class _FingerPrintAuthPageState extends State<FingerPrintAuthPage> {
  FingerPrintAuthBloc _block;

  @override
  void initState() {
    try {
      _block = BlocProvider.getBloc<FingerPrintAuthBloc>();
    } catch (e) {}
    super.initState();
  }

  void tryToAuth() async {
    var auth = await _block.startAuth();
    if (auth) {
      Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(
      child: Consumer<FingerPrintAuthBloc>(
        builder: (BuildContext context, FingerPrintAuthBloc _block) {
          if (!_block.isAuthenticating && !_block.authenticated) {
            tryToAuth();
          }
          return LoadingWidget();
        },
      ),
    ));
  }
}
