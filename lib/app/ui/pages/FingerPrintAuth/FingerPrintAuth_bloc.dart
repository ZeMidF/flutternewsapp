import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:local_auth/local_auth.dart';

class FingerPrintAuthBloc extends BlocBase {
  final LocalAuthentication auth = LocalAuthentication();
  bool authenticated = false;
  bool isAuthenticating = false;

  Future<bool> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on Exception catch (e) {
      print(e);
    }
    return canCheckBiometrics;
  }

  Future<bool> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on Exception catch (e) {
      print(e);
    }

    if (availableBiometrics.contains((null))) {}

    return true;
  }

  Future<bool> _authenticate() async {
    try {
      if (!isAuthenticating) {
        isAuthenticating = true;
        authenticated =
            await auth.authenticateWithBiometrics(localizedReason: 'Scan your fingerprint to authenticate', useErrorDialogs: true, stickyAuth: true);
        return authenticated;
      }
    } on Exception catch (e) {
      print(e);
      authenticated = false;
    } finally {
      isAuthenticating = false;
      notifyListeners();
    }
    return authenticated;
  }

  Future<bool> startAuth() async {
    bool authServicesAvailable;

    try {
      authServicesAvailable = await _checkBiometrics() && await _getAvailableBiometrics();
    } catch (e) {
      authServicesAvailable = false;
    }

    try {
      // Can log with finger
      if (authServicesAvailable) {
        return await _authenticate();
      } else {
        // AvailableBiometrics are disable..must return true soo flow can go on
        authenticated = true;
        isAuthenticating = false;
        notifyListeners();
      }
    } catch (e) {}
    return authenticated;
  }

  void cancelAuth() async {
    try {
      isAuthenticating = false;
      authenticated = false;
      auth.stopAuthentication();
    } catch (e) {} finally {
      notifyListeners();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
