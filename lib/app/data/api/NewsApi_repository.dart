import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:dio/dio.dart';
import 'package:flutternewsapp/app/data/model/NewsApiArticleModel_model.dart';
import 'package:flutternewsapp/app/data/model/NewsApiSourceModel_model.dart';
import 'package:flutternewsapp/app/res/contants.dart';

abstract class NewsApiRepository {
  Future<List<Articles>> getNewsBySource(String sourceTag);
  Future<List<Sources>> getSources();
}

class NewsApiRepositoryImpl extends Disposable implements NewsApiRepository {
  //dispose will be called automatically
  @override
  void dispose() {}

  @override
  Future<List<Articles>> getNewsBySource(String sourceTag) async {
    String url = "${AppVariables.newsapiNewsBySourceUrl}${AppVariables.newsapiAPIKey}".replaceAll("#REPLACETAG#", sourceTag);
    var response = await Dio().get(url);

    if (response.statusCode == 200) {
      var articles = NewsApiArticleModel.fromJson(response.data).articles;
      return articles;
    } else {
      throw Exception();
    }
  }

  @override
  Future<List<Sources>> getSources() async {
    String url = "${AppVariables.newsapiSourcesUrl}${AppVariables.newsapiAPIKey}";
    var response = await Dio().get(url);

    if (response.statusCode == 200) {
      var source = NewsApiSourceModel.fromJson(response.data).sources;
      return source;
    } else {
      throw Exception();
    }
  }
}
