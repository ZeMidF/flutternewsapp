import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData currentTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.red,
    textTheme: TextTheme(
      headline: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
      title: TextStyle(fontSize: 20.0, color: Colors.white),
      body1: TextStyle(fontSize: 18.0),
      body2: TextStyle(fontSize: 14.0),
      caption: TextStyle(fontStyle: FontStyle.italic),
    ),
  );
}
