import 'package:flutter/material.dart';
import 'package:flutternewsapp/app/data/model/NewsApiArticleModel_model.dart';
import 'package:flutternewsapp/app/ui/pages/FingerPrintAuth/FingerPrintAuth_page.dart';
import 'package:flutternewsapp/app/ui/pages/NewsDetail/NewsDetail_page.dart';
import 'package:flutternewsapp/app/ui/pages/NewsHomeScreen/NewsHomeScreen_page.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => NewsHomeScreenPage());
      case '/Auth':
        return MaterialPageRoute(builder: (_) => FingerPrintAuthPage());
      case '/NewsDetais':
        if (args is Articles) {
          return MaterialPageRoute(
            builder: (_) => NewsDetailPage(
              currentArticle: args,
            ),
          );
        }
        return _errorRoute();
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('No route defined.'),
        ),
      );
    });
  }
}
