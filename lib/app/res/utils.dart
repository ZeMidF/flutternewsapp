import 'package:intl/intl.dart';

class Utils {
  static String getDateString(String date, {String dateFormat = 'yyyy-MM-dd HH:mm'}) {
    try {
      var time = DateTime.parse(date);
      var formatter = DateFormat(dateFormat);
      String formatted = formatter.format(time);
      return formatted;
    } catch (e) {}
    return "";
  }

  static int getDiferenceToCurrentInHours(DateTime date) {
    try {
      return DateTime.now().difference(date).inHours;
    } catch (e) {}
    return 0;
  }

  static DateTime getDateTimeFromString(String date) {
    try {
      var time = DateTime.parse(date);
      return time;
    } catch (e) {}
    return DateTime.now();
  }
}
