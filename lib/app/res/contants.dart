class AppVariables {
  static const String newsapiSourcesUrl = "https://newsapi.org/v2/sources?language=en&country=us&apiKey=";
  static const String newsapiNewsBySourceUrl = "https://newsapi.org/v2/top-headlines?sources=#REPLACETAG#&sortBy=publishedAt&apiKey=";
  static const String newsapiAPIKey = "a14b0cb14640495fa6c3d0884e98ec80";
}
