class AppStrings {
  static const String goToFullArticle = "Go to Full Article";
  static const String justNow = "Just now";
  static const String hoursAgo = "hours ago";
  static const String newsHomeScreenTitle = "News";
}
