import 'package:bloc_pattern/bloc_pattern_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutternewsapp/app/res/strings.dart';
import 'package:flutternewsapp/app/ui/widgets/NewsCard_widget.dart';

main() {
  testWidgets('NewsCard without parameters should return "Just Now" tag', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(NewsCard()));
    final justNowTextFinder = find.text(AppStrings.justNow);
    expect(justNowTextFinder, findsOneWidget);
  });

  testWidgets('NewsCard with DateTime.Now + 3hours should return "3 hours ago" ', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(NewsCard(
      date: DateTime.now().add(Duration(hours: -3, minutes: -1)),
    )));
    final justNowTextFinder = find.text("3 " + AppStrings.hoursAgo);
    expect(justNowTextFinder, findsOneWidget);
  });
}
