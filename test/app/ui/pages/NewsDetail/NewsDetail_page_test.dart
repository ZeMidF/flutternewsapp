import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_pattern/bloc_pattern_test.dart';
import 'package:flutternewsapp/app/data/model/NewsApiArticleModel_model.dart';
import 'package:flutternewsapp/app/ui/pages/NewsDetail/NewsDetail_page.dart';

main() {
  testWidgets('The NewsDetailPage title should be the same as the passed News', (WidgetTester tester) async {
    String newsTestTitle = "Article Title Test";
    Articles newsTest = Articles(source: Source(name: newsTestTitle));
    await tester.pumpWidget(buildTestableWidget(NewsDetailPage(
      currentArticle: newsTest,
    )));
    final titleFinder = find.text(newsTestTitle);
    expect(titleFinder, findsOneWidget);
  });
}
