import 'package:flutternewsapp/app/data/api/NewsApi_repository.dart';
import 'package:test/test.dart';

main() {
  group('Get News Tests', () {
    test('Have at least one News Source', () async {
      final newsApi = NewsApiRepositoryImpl();

      var sources = await newsApi.getSources();

      expect(sources.length > 0, true);
    });

    test('Have at least one New from a News Source', () async {
      final newsApi = NewsApiRepositoryImpl();

      var sources = await newsApi.getSources();

      expect(sources.length > 0, true);

      var news = await newsApi.getNewsBySource(sources[0].id);

      expect(news.length > 0, true);
    });
  });
}
